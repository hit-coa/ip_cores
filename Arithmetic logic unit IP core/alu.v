`timescale 1ns / 1ps

`include "../Head_Files/Instructions.vh"
`include "../Head_Files/Parameters.vh"
`include "../Head_Files/Parameters_AL.vh"

/**
 *  We will use combinatory logic to implement a part of the ALU circuit.
 *  The other part of ALU, which using sequential logic will be given separately.
 */
module alu(
    input      `N(`INS_LEN ) instr,
    input      `N(`W       ) rs1,    // the first value
    input      `N(`W       ) rs2,    // the second value, including imm and sa.


    output reg `N(`W       ) res,
    output reg               of      // Signal Exception: Integer Overflow
);

    wire                     overflow;
    wire       `N(`AL_TOTAL) option;
    wire       `N(`W       ) result;
    wire       `N(`OP_LEN  ) op     = instr[31:26];
    wire       `N(`FUN_LEN ) func   = instr[ 5: 0];
    wire                        ext_add;
    // ext: extended sign
    wire       `N(`W       ) result_add;
    // wire       `N(`W       ) result_addu;
    wire                        ext_sub;
    wire       `N(`W       ) result_sub;
    // wire       `N(`W       ) result_subu;
    // wire       `N(`W       ) result_slt;
    wire       `N(`W       ) result_and;
    wire       `N(`W       ) result_nor;
    wire       `N(`W       ) result_or ;
    wire       `N(`W       ) result_xor;
    wire       `N(`W       ) result_sll;
    wire       `N(`W       ) result_sra;
    wire       `N(`W       ) result_srl;
    wire       `N(`W       ) result_sllv;
    wire       `N(`W       ) result_srav;
    wire       `N(`W       ) result_srlv;
    wire       `N(`W       ) result_lui;

    wire                     us        ; // Unsigned


    /* Choose an ALU option according to operate code and function code. */
    assign option[`AL_ADD]  = {op, func} == {`ADD  , `FUN_ADD}
                            ||  op        ==  `ADDI
                            ||  op        ==  `ADDIU
                            || {op, func} == {`ADDU , `FUN_ADDU};
    assign option[`AL_SUB]  = {op, func} == {`SUB  , `FUN_SUB}
                            || {op, func} == {`SUBU , `FUN_SUBU};
    assign option[`AL_SLT]  = {op, func} == {`SLT  , `FUN_SLT}
                            ||  op        ==  `SLTI
                            || {op, func} == {`SLTU , `FUN_SLTU}
                            ||  op        ==  `SLTIU;
    assign option[`AL_AND]  = {op, func} == {`AND  , `FUN_AND}
                            ||  op        ==  `ANDI;
    assign option[`AL_NOR]  = {op, func} == {`NOR  , `FUN_NOR};
    assign option[`AL_OR ]  = {op, func} == {`OR   , `FUN_OR}
                            ||  op        ==  `ORI;
    assign option[`AL_XOR]  = {op, func} == {`XOR  , `FUN_XOR}
                            ||  op        ==  `XORI;
    assign option[`AL_SLL]  = {op, func} == {`SLL  , `FUN_SLL};
    assign option[`AL_SRA]  = {op, func} == {`SRA  , `FUN_SRA};
    assign option[`AL_SRL]  = {op, func} == {`SRL  , `FUN_SRL};
    assign option[`AL_SLLV] = {op, func} == {`SLLV , `FUN_SLLV};
    assign option[`AL_SRAV] = {op, func} == {`SRAV , `FUN_SRAV};
    assign option[`AL_SRLV] = {op, func} == {`SRLV , `FUN_SRLV};
    assign option[`AL_LUI]  = {op, instr[25:21]} == {`LUI,5'b0};

    assign us               = {op, func} == {`ADDU , `FUN_ADDU}
                            ||  op        ==  `ADDIU
                            || {op, func} == {`SUBU , `FUN_SUBU}
                            || {op, func} == {`SLTU , `FUN_SLTU}
                            ||  op        ==  `SLTIU            ;

    /* Calculate results in all options. */
    assign {ext_add, result_add} = $signed(rs1) + $signed(rs2);
    // assign           result_addu =         rs1  +         rs2 ;
    assign {ext_sub, result_sub} = $signed(rs1) - $signed(rs2);
    // assign           result_subu =         rs1  -         rs2 ;
    // assign           result_slt  = $signed(rs1) < $signed(rs2);
    assign           result_sltu =         rs1  <         rs2       ;
    assign           result_and  =         rs1  &         rs2       ;
    assign           result_nor  = ~result_or                       ;
    assign           result_or   =         rs1  |         rs2       ;
    assign           result_xor  =         rs1  ^         rs2       ;
    assign           result_sll  =         rs2  <<        rs1       ;
    assign           result_sra  = $signed(rs2) >>>       rs1       ;
    assign           result_srl  =         rs2  >>        rs1       ;
    assign           result_sllv =         rs2  <<        rs1[4:0]  ;
    assign           result_srav = $signed(rs2) >>>       rs1[4:0]  ;
    assign           result_srlv =         rs2  >>        rs1[4:0]  ;
    assign           result_lui  =         rs2  <<        16        ;

    /* Get the last result according to given option and calculated results. */
    assign result = option[`AL_ADD ] ? result_add  :
                    option[`AL_SUB ] ? result_sub  :
                    option[`AL_SLT ] ? (us ? result_sltu/*result_sub[`W-1]*/ : ext_sub) :
                    option[`AL_AND ] ? result_and  :
                    option[`AL_NOR ] ? result_nor  :
                    option[`AL_OR  ] ? result_or   :
                    option[`AL_XOR ] ? result_xor  :
                    option[`AL_SLL ] ? result_sll  :
                    option[`AL_SRA ] ? result_sra  :
                    option[`AL_SRL ] ? result_srl  :
                    option[`AL_SLLV] ? result_sllv :
                    option[`AL_SRAV] ? result_srav :
                    option[`AL_SRLV] ? result_srlv :
                    option[`AL_LUI ] ? result_lui  : 0;
    assign overflow = !us && (
                        (option[`AL_ADD] && ext_add != result_add[`W-1]) ||
                        (option[`AL_SUB] && ext_sub != result_sub[`W-1])
                    );


    always @* begin
        of <= overflow;
        res <= result;
    end

endmodule // alu