`timescale 1ns / 1ps

`include "./cpu.vh"

module d_memory(
    input             reset,
    input             clk,
    input             wen,
    input      [31:0] addr,
    input      [31:0] wdata,
    output     [31:0] rdata
);

    reg [31:0] memory [0:255];

	 initial begin
		$readmemh(`DATA_FILE_PATH , memory);
	 end

    always @ (posedge clk) begin
        if (wen) begin
            memory[addr] <= wdata;
        end
        else begin end
    end

    assign rdata = memory[addr];

endmodule