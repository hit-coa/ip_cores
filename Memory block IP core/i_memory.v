`timescale 1ns / 1ps

`include "./cpu.vh"

module i_memory(
    input             en,
    input             reset,
    input             clk,
    input      [31:0] addr,
    output reg [31:0] data
);

    reg [31:0] memory [0:255];
	 
	 initial begin
		$readmemh(`INST_FILE_PATH , memory);
	 end

    always @ (posedge clk) begin
        if(reset) begin
            data <= 32'b0;
        end
        else if(en) begin
            data <= memory[addr[31:2]];
        end
        else begin end
    end

endmodule
