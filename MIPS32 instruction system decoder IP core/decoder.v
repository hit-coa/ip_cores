`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/02/17 22:48:40
// Design Name: 
// Module Name: decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
//      Decoder
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "../Head_Files/Instructions.vh"
`include "../Head_Files/Parameters.vh"
`include "../Head_Files/Parameters_decoder.vh"

module decoder
    #(
        parameter IMM_EXT_BIT    = 16,          //Extend 16-bit immediate operand to 32-bit
        parameter SA_EXT_BIT     = 27,          //Extend 5-bit sa immediate operand to 32-bit
        parameter JBR_CTRL_WIDTH = 10,          //Width of branch unit control signal
        parameter MEM_CTRL_WIDTH = 8,           //Width of branch unit control signal
        parameter SA_LEN         = 5,           //The length of immediate in shift instruction
        parameter SEL_LEN        = 3,           //Immediate in shift instruction
        parameter ZERO           = 0            //Zero, used to check if some fields of IR are 0(including rs,rt,rd,sa,func[5:3])
    )
(
    //----------Outputs----------
    //-----ID signals-----
    output [`ADDR_LEN-1:0]      addr_reg_a,     //->GPRS,as reg_a addresses to read data from gprs
    output [`ADDR_LEN-1:0]      addr_reg_b,     //->GPRS,as reg_b addresses to read data from gprs
    //-----EX signals-----
    output [JBR_CTRL_WIDTH-1:0] jbr_ctrl,       //->jump&branch unit,branch_control,give the branch type of the current decode instruction
    output                      alu_op1_mux_sel,
    output                      alu_op2_mux_sel,//->alu mux,select the second operand of alu from b/imm(sa); 0 -> b ; 1 -> imm/sa
    output                      mthi,           //->hi/lo mux,select the data to be transferred to hi,0 -> reg_a; 1 -> alu_out_hi
    output                      mtlo,           //->hi/lo mux,select the data to be transferred to lo,0 -> reg_a; 1 -> alu_out_lo
    output                      div,
    output                      divu,
    output                      mul,
    output                      mult,
    output                      multu,
    output                      madd,
    output                      maddu,
    output                      msub,
    output                      msubu,

    //-----MEM signals-----
    output [MEM_CTRL_WIDTH-1:0] mem_ctrl,       //->d_cache,mem_control,give the memory access type of the current decode instruction
    //-----WB signals-----
    output                      link,           //->regfile write back unit,link flag,identify whether write pc back to the regfiles
    output                      rf_wen,         //->regfile write back unit,regfile_write_enable,1 -> need to write back
    output [`ADDR_LEN-1:0]      rf_waddr,       //->regfile write back unit,regfile_write_address,Address of register to be written back
    output                      hi_wen,         //->HI/LO register,hi register write enable signal
    output                      lo_wen,         //->HI/LO register,lo register write enable signal
    output                      mfhi,           //->HI/LO register,read from hi register,and write read data back to the regfiles
    output                      mflo,           //->HI/LO register,read from lo register,and write read data back to the regfiles
    output                      mtc0,           //->CP0 register,identify whether read from CP0,and write read data back to the regfiles
    output                      mfc0,           //->CP0 register,identify whether write into CP0
    output [SA_LEN+SEL_LEN-1:0] cp0_addr,       //->CP0 register,CP0_read_address,identify which CP0 register to read from or write into
    //output reg                  is_in_delayslot,//->CP0 register,CP0_read_address,identify which CP0 register to read from or write into
    output                      eret,           //->Exception handler,return from exception handler
    //-----exception-----
    output                      exc_ri,         //->Exception detection,reserved instruction exception
    output                      exc_sys,        //->Exception detection,system call exception
    output                      exc_bp,         //->Exception detection,break point exception
    output                      is_jbr,
    output                      is_lui,
    output [4:0]                unextend_sa,
    output [15:0]               unextend_imm,
    output                      unsigned_imm_taken,
    output                      cache_req,
    output [4 :0]               cache_op,
    output                      likely,
    output                      movz,
    output                      movn,
    output                      teq,
    output                      teqi,
    output                      tge,
    output                      tgei,
    output                      tgeiu,
    output                      tgeu,
    output                      tlt,
    output                      tlti,
    output                      tltiu,
    output                      tltu,
    output                      tne,
    output                      tnei,
    output                      lwl,
    output                      lwr,
    output                      swl,
    output                      swr,
    output                      ll,
    output                      sc,
    output                      clo,
    output                      clz,
    output                      tlbp,
    output                      tlbr,
    output                      tlbwi,
    output                      tlbwr,
    output                      ins_wait,
    output                      movft,
    output                      cop1,
    output                      lwc1,
    output                      ldc1,
    output                      swc1,
    output                      sdc1,
    //----------Inputs----------
    input  [`REG_WIDTH-1:0]     ir
    );

    //---------------------------------------INPUT SIGNAL PREPROCCESSING---------------------------------------
    //----------Split ir signal----------
    //Split the ir signal according to the format,and get the value of each field
    wire [`OP_LEN-1:0  ]    op      =       ir[`IR_OP  ];
    wire [`FUN_LEN-1:0 ]    func    =       ir[`IR_FUNC];
    wire [`ADDR_LEN-1:0]    rs      =       ir[`IR_RS  ];
    wire [`ADDR_LEN-1:0]    rt      =       ir[`IR_RT  ];
    wire [`ADDR_LEN-1:0]    rd      =       ir[`IR_RD  ];
    wire [SA_LEN-1:0   ]    sa      =       ir[`IR_SA  ];
    wire [`IMM_LEN-1:0 ]    imm     =       ir[`IR_IMM ];
    wire [SEL_LEN-1:0  ]    sel     =       ir[`IR_SEL ];//cp0 register select signal


    //----------Instruction type identification----------
    //If input IR matches the format of a certain instruction, the corresponding ins signal is assigned a value of 1.
    //For example, if it matches the add instruction format, "ins_add" is assigned 1, and other signal is assigned 0.
    //Used to identify which type the current decode instruction is.
    //-----I-Class Instructions
    //Arithmetic Operation Instruction
    wire ins_addi     = (op ==`ADDI);
    wire ins_addiu    = (op ==`ADDIU);
    wire ins_slti     = (op ==`SLTI);
    wire ins_sltiu    = (op ==`SLTIU);
    //Logical Operation Instruction
    wire ins_andi     = (op ==`ANDI);
    wire ins_lui      = (op ==`LUI);
    wire ins_ori      = (op ==`ORI);
    wire ins_xori     = (op ==`XORI);
    //Conditional Branch Instruction
    wire ins_beq      = (op ==`BEQ);
    wire ins_bne      = (op ==`BNE);
    wire ins_bgez     = (op ==`BGEZ)    && (rt == `RT_BGEZ);
    wire ins_bgtz     = (op ==`BGTZ)    && (rt == ZERO);
    wire ins_blez     = (op ==`BLEZ)    && (rt == ZERO);
    wire ins_bltz     = (op ==`BLTZ)    && (rt == ZERO);
    wire ins_bgezal   = (op ==`BGEZAL)  && (rt == `RT_BGEZAL);
    wire ins_bltzal   = (op ==`BLTZAL)  && (rt == `RT_BLTZAL);
    //Memory Access Instruction
    wire ins_lb       = (op ==`LB);
    wire ins_lbu      = (op ==`LBU);
    wire ins_lh       = (op ==`LH);
    wire ins_lhu      = (op ==`LHU);
    wire ins_lw       = (op ==`LW);
    wire ins_sb       = (op ==`SB);
    wire ins_sh       = (op ==`SH);
    wire ins_sw       = (op ==`SW);
    //-----R-Class Instructions
    //Arithmetic Operation Instruction
    wire ins_add      = (op ==`ADD)     && (func == `FUN_ADD)    && (sa == ZERO);
    wire ins_addu     = (op ==`ADDU)    && (func == `FUN_ADDU)   && (sa == ZERO);
    wire ins_sub      = (op ==`SUB)     && (func == `FUN_SUB)    && (sa == ZERO);
    wire ins_subu     = (op ==`SUBU)    && (func == `FUN_SUBU)   && (sa == ZERO);
    wire ins_slt      = (op ==`SLT)     && (func == `FUN_SLT)    && (sa == ZERO);
    wire ins_sltu     = (op ==`SLTU)    && (func == `FUN_SLTU)   && (sa == ZERO);
    //Arithmetic Operation Instruction(mult and div)
    wire ins_div      = (op ==`DIV)     && (func == `FUN_DIV)    && (rd == ZERO) && (sa == ZERO);
    wire ins_divu     = (op ==`DIVU)    && (func == `FUN_DIVU)   && (rd == ZERO) && (sa == ZERO);
    wire ins_mult     = (op ==`MULT)    && (func == `FUN_MULT)   && (rd == ZERO) && (sa == ZERO);
    wire ins_multu    = (op ==`MULTU)   && (func == `FUN_MULTU)  && (rd == ZERO) && (sa == ZERO);
    wire ins_mul      = (op ==`MUL)     && (func == `FUN_MUL)    && (sa == ZERO);
    //Logical Operation Instruction
    wire ins_and      = (op ==`AND)     && (func == `FUN_AND)    && (sa == ZERO);
    wire ins_nor      = (op ==`NOR)     && (func == `FUN_NOR)    && (sa == ZERO);
    wire ins_or       = (op ==`OR)      && (func == `FUN_OR)     && (sa == ZERO);
    wire ins_xor      = (op ==`XOR)     && (func == `FUN_XOR)    && (sa == ZERO);
    //Shift Operation Instruction
    wire ins_sllv     = (op ==`SLLV)    && (func == `FUN_SLLV)   && (sa == ZERO);
    wire ins_sll      = (op ==`SLL)     && (func == `FUN_SLL)    && (rs == ZERO);
    wire ins_srav     = (op ==`SRAV)    && (func == `FUN_SRAV)   && (sa == ZERO);
    wire ins_sra      = (op ==`SRA)     && (func == `FUN_SRA)    && (rs == ZERO);
    wire ins_srlv     = (op ==`SRLV)    && (func == `FUN_SRLV)   && (sa == ZERO);
    wire ins_srl      = (op ==`SRL)     && (func == `FUN_SRL)    && (rs == ZERO);
    //UNconditional Branch Instruction
    wire ins_jr       = (op ==`JR)      && (func == `FUN_JR)     && (rt == ZERO) && (rd == ZERO) && (sa == ZERO);
    wire ins_jalr     = (op ==`JR)      && (func == `FUN_JALR)   && (rt == ZERO) && (sa == ZERO);
    //Data Movement Instruction
    wire ins_mfhi     = (op ==`MFHI)    && (func == `FUN_MFHI)   && (rs == ZERO) && (rt == ZERO) && (sa == ZERO);
    wire ins_mflo     = (op ==`MFLO)    && (func == `FUN_MFLO)   && (rs == ZERO) && (rt == ZERO) && (sa == ZERO);
    wire ins_mthi     = (op ==`MTHI)    && (func == `FUN_MTHI)   && (rt == ZERO) && (rd == ZERO) && (sa == ZERO);
    wire ins_mtlo     = (op ==`MTLO)    && (func == `FUN_MTLO)   && (rt == ZERO) && (rd == ZERO) && (sa == ZERO);
    //Self-trap Instruction
    wire ins_break    = (op ==`BREAK)   && (func == `FUN_BREAK);
    wire ins_syscall  = (op ==`SYSCALL) && (func == `FUN_SYSCALL);
    //-----J-Class Instructions
    //Unconditional Branch Instruction
    wire ins_j        = (op ==`J);
    wire ins_jal      = (op ==`JAL);
    //-----Privileged Instructions
    wire ins_eret     = (ir == `INS_ERET);
    wire ins_mfc0     = (op ==`MFC0)    && (rs == ZERO)         && (sa == ZERO) && (func[`FUNC_HI] == ZERO);
    wire ins_mtc0     = (op ==`MTC0)    && (rs == `RS_MTC0)     && (sa == ZERO) && (func[`FUNC_HI] == ZERO);

    //TODO:
    //Likely brach instruction
    wire ins_beql     = (op == `BEQL);
    wire ins_bgezall  = (op == `BGEZALL) && (rt == 5'b10010);
    wire ins_bgezl    = (op == `BGEZL)   && (rt == 5'b00011);
    wire ins_bgtzl    = (op == `BGTZL)   && (rt == 5'b00000);
    wire ins_blezl    = (op == `BLEZL)   && (rt == 5'b00000);
    wire ins_bltzall  = (op == `BLTZALL) && (rt == 5'b10010);
    wire ins_bltzl    = (op == `BLTZL)   && (rt == 5'b00010);
    wire ins_bnel     = (op == `BNEL);

    wire ins_cache    = (op == `CACHE);

    wire ins_clo      = (op == `CLO)    && (sa == ZERO) && (func == `FUN_CLO);
    wire ins_clz      = (op == `CLZ)    && (sa == ZERO) && (func == `FUN_CLZ);

    wire ins_ll       = (op == `LL);
    wire ins_sc       = (op == `SC);

    wire ins_lwl      = (op == `LWL);
    wire ins_lwr      = (op == `LWR);

    wire ins_swl      = (op == `SWL);
    wire ins_swr      = (op == `SWR);

    wire ins_madd     = (op == `MADD)    && (rd == ZERO) && (sa == ZERO) && (func == `FUN_MADD);
    wire ins_maddu    = (op == `MADDU)   && (rd == ZERO) && (sa == ZERO) && (func == `FUN_MADDU);

    wire ins_msub     = (op == `MSUB)    && (rd == ZERO) && (sa == ZERO) && (func == `FUN_MSUB);
    wire ins_msubu    = (op == `MSUBU)   && (rd == ZERO) && (sa == ZERO) && (func == `FUN_MSUBU);

    wire ins_movn     = (op == `MOVN)    && (sa == ZERO) && (func == `FUN_MOVN);
    wire ins_movz     = (op == `MOVZ)    && (sa == ZERO) && (func == `FUN_MOVZ);

    wire ins_perf     = (op == `PREF);
    wire ins_sync     = (op == `SYNC)    && (rs == ZERO) && (rt == ZERO) && (rd == ZERO) && (func == `FUN_SYNC);
    assign ins_wait     = (op == `WAIT)    && (ir[25] == 1'b1) && (func == `FUN_WAIT);

    wire ins_teq      = (op == `TEQ)     && (func == `FUN_TEQ);
    wire ins_teqi     = (op == `TEQI)    && (rt == 5'b01100);
    wire ins_tge      = (op == `TGE)     && (func == `FUN_TGE);
    wire ins_tgei     = (op == `TGEI)    && (rt == 5'b01000);
    wire ins_tgeiu    = (op == `TGEIU)   && (rt == 5'b01001);
    wire ins_tgeu     = (op == `TGEU)    && (func == `FUN_TGEU);
    wire ins_tlt      = (op == `TLT)     && (func == `FUN_TLT);
    wire ins_tlti     = (op == `TLTI)    && (rt == 5'b01010);
    wire ins_tltiu    = (op == `TLTIU)   && (rt == 5'b01011);
    wire ins_tltu     = (op == `TLTU)    && (func == `FUN_TLTU);
    wire ins_tne      = (op == `TNE)     && (func == `FUN_TNE);
    wire ins_tnei     = (op == `TNEI)    && (rt == 5'b01110);

    wire ins_tlbp     = (op == `TLBP)    && (rs == 5'b10000) && (rt == ZERO) && (rd == ZERO)  && (sa == ZERO) && (func == `FUN_TLBP);
    wire ins_tlbr     = (op == `TLBR)    && (rs == 5'b10000) && (rt == ZERO) && (rd == ZERO)  && (sa == ZERO) && (func == `FUN_TLBR);
    wire ins_tlbwi    = (op == `TLBWI)   && (rs == 5'b10000) && (rt == ZERO) && (rd == ZERO)  && (sa == ZERO) && (func == `FUN_TLBWI);
    wire ins_tlbwr    = (op == `TLBWR)   && (rs == 5'b10000) && (rt == ZERO) && (rd == ZERO)  && (sa == ZERO) && (func == `FUN_TLBWR);

    wire ins_movft    = (op == 6'b000000) && (sa == ZERO) && (func == 6'b000001);
    wire ins_cop1     = (op == 6'b010001) && (rs != 5'b01110);
    wire ins_lwc1     = (op == 6'b110001);
    wire ins_ldc1     = (op == 6'b110101);
    wire ins_swc1     = (op == 6'b111001);
    wire ins_sdc1     = (op == 6'b111101);
    //-----------------------------------------------------------------------------------------------------------

    assign cache_req  = ins_cache;
    assign cache_op   = rt;

    assign madd       = ins_madd;
    assign maddu      = ins_maddu;
    assign msub       = ins_msub;
    assign msubu      = ins_msubu;

    assign movz       = ins_movz;
    assign movn       = ins_movn;


    assign teq        = ins_teq;
    assign teqi       = ins_teqi;
    assign tge        = ins_tge;
    assign tgei       = ins_tgei;
    assign tgeiu      = ins_tgeiu;
    assign tgeu       = ins_tgeu;
    assign tlt        = ins_tlt;
    assign tlti       = ins_tlti;
    assign tltiu      = ins_tltiu;
    assign tltu       = ins_tltu;
    assign tne        = ins_tne;
    assign tnei       = ins_tnei;

    assign lwl        = ins_lwl;
    assign lwr        = ins_lwr;
    assign swl        = ins_swl;
    assign swr        = ins_swr;

    assign ll         = ins_ll;
    assign sc         = ins_sc;

    assign clo        = ins_clo;
    assign clz        = ins_clz;

    assign tlbp       = ins_tlbp;
    assign tlbr       = ins_tlbr;
    assign tlbwi      = ins_tlbwi;
    assign tlbwr      = ins_tlbwr;

    assign movft      = ins_movft;
    assign cop1       = ins_cop1;
    assign lwc1       = ins_lwc1;
    assign ldc1       = ins_ldc1;
    assign swc1       = ins_swc1;
    assign sdc1       = ins_sdc1;


    //---------------------------------------ID SEGMENT SIGNAL PROCCESSING---------------------------------------
    //----------Send to the GPRS read unit----------
    //Read the reg_a/b addresses from IR
    //if it's a shift instruction, the operand of alu is oppsite.
    assign addr_reg_a       =   rs;
    assign addr_reg_b       =   rt;

    //----------Send to the branch prediction and jump unit----------
    //j_valid:uesd to identify whether the current decode instruction is a j/jal instruction
    //jr_valid:uesd to identify whether the current decode instruction is a jr/jalr instruction
    //jbr_ctrl:jump and branch_control,give the branch type of the currently decoding instruction
    //The branch unit will make the judgment by branch type information
    wire   j_valid          =   ins_j      || ins_jal;
    wire   jr_valid         =   ins_jr     || ins_jalr;
    wire   beq              =   ins_beq    || ins_beql;
    wire   bne              =   ins_bne    || ins_bnel;
    wire   bgez             =   ins_bgez   || ins_bgezl;
    wire   bgtz             =   ins_bgtz   || ins_bgtzl;
    wire   blez             =   ins_blez   || ins_blezl;
    wire   bltz             =   ins_bltz   || ins_bltzl;
    wire   bgezal           =   ins_bgezal || ins_bgezall;
    wire   bltzal           =   ins_bltzal || ins_bltzall;
    assign likely           =   ins_beql || ins_bnel|| ins_bgezl || ins_bgtzl || ins_blezl || ins_bltzl || ins_bgezall || ins_bltzall;
    assign is_jbr           =   beq || bne || bgez || bgtz || blez || bltz || bgezal || bltzal || j_valid || jr_valid;
    assign jbr_ctrl         =   {beq , bne  , bgez   , bgtz   ,
                                blez , bltz , bgezal , bltzal ,
                                j_valid     , jr_valid  };
    //-----------------------------------------------------------------------------------------------------------





    //---------------------------------------EX SEGMENT SIGNAL PROCCESSING---------------------------------------
    //----------Immediate extension----------
    //"sa_taken" means choose "sa" as immediate
    //"unsigned_taken" means choose "unsigned extended imm" as immediate
    assign unextend_sa = sa;
    assign unextend_imm = imm;
    //wire   sa_taken             =   ins_sll  || ins_sra || ins_srl  ;
    assign unsigned_imm_taken   =   ins_andi || ins_ori || ins_xori ;
    /*assign extend_imm           =   sa_taken        ? {{SA_EXT_BIT{1'b0}}  , sa } :   //sa  -> {27'b0 ,[15:0]}
                                    unsigned_taken  ? {{IMM_EXT_BIT{1'b0}} , imm} :   //Imm -> {16'b0 ,[15:0]}
                                    {{IMM_EXT_BIT{imm[`IMM_LEN-1]}} , imm}        ;   //Imm -> {16[15],[15:0]}*/
    //-------------------------------------------------------------------------------------------------

    //-----------Send to the ALU oprand mux----------
    //The signal used to select the second operand of alu from b/imm; 0 -> b ; 1 -> imm
    //Notice:although sll,sra,srl use "sa" as imm,they use the same data path as imm.So they belong to "imm_taken"
    //mux2:0 -> b ; 1 -> imm
    assign alu_op2_mux_sel      =   ins_addi || ins_addiu || ins_slti   || ins_sltiu  ||
                                    ins_andi || ins_lui   || ins_ori    || ins_xori   ||
                                    ins_beq  || ins_bne   || ins_bgtz   || ins_blez   ||
                                    ins_bgez || ins_bltz  || ins_bgezal || ins_bltzal ||
                                    ins_beql || ins_bnel  || ins_bgtzl  || ins_blezl  ||
                                    ins_bgezl|| ins_bltzl || ins_bgezall|| ins_bltzall||
                                    ins_lb   || ins_lbu   || ins_lh     || ins_lhu    ||
                                    ins_lw   || ins_sb    || ins_sh     || ins_sw     ||
                                    ins_ll   || ins_lwl   || ins_lwr    || ins_sc     ||
                                    ins_swl  || ins_swr    ;
    //mux1:0 -> a ; 1 -> sa
    assign alu_op1_mux_sel      =   ins_sll  || ins_srl   || ins_sra     ;

    //-----------Send to the ALU operation selection----------
    assign div                  =   ins_div;
    assign divu                 =   ins_divu;
    assign mul                  =   ins_mul;
    assign mult                 =   ins_mult;
    assign multu                =   ins_multu;

    assign is_lui = ins_lui;


    //----------Send to the HI/LO register----------
    //hi/lo_wen:hi/lo register write enable signal
    //mfhi,mflo:control to read from hi/lo register
    //mthi,mtlo:control to write into hi/lo register
    assign hi_wen               =   ins_div || ins_divu || ins_mult || ins_multu || ins_mthi|| ins_madd || ins_maddu || ins_msub || ins_msubu;
    assign lo_wen               =   ins_div || ins_divu || ins_mult || ins_multu || ins_mtlo|| ins_madd || ins_maddu || ins_msub || ins_msubu;
    assign mfhi                 =   ins_mfhi;
    assign mflo                 =   ins_mflo;
    assign mthi                 =   ins_mthi;
    assign mtlo                 =   ins_mtlo;
    //-----------------------------------------------------------------------------------------------------------





    //---------------------------------------MEM SEGMENT SIGNAL PROCCESSING---------------------------------------
    //----------send to the d_cache----------
    //mem_control:give the memory access type of the current decode instruction,finally used by d_cache in MEM segment
    assign mem_ctrl             =  {ins_lb , ins_lbu , ins_lh , ins_lhu , ins_lw||ins_ll , ins_sb , ins_sh , ins_sw};
    //------------------------------------------------------------------------------------------------------------





    //---------------------------------------WB SEGMENT SIGNAL PROCCESSING---------------------------------------
    //----------Send to regfile write back unit----------
    //link:used to identify the data that needs to be written back is pc,not lmd/alu_output/hi/lo/CP0
    //rf_wen:regfile_write_enable; Write enable signal,1 -> need to write back to regfiles
    //rf_waddr:regfile_write_address; Address of register to be written back
    //use rf_waddr_rt/rd/al to identify Which field of IR is the real register write-back address
    //such as:"rf_waddr_rt = 1" means the register address identified by rt is write-back address
    //"rf_waddr_al" means "link",write-back register will be R31
    wire   rf_waddr_rt  =   ins_addi    || ins_addiu   || ins_slti || ins_sltiu ||
                            ins_andi    || ins_lui     || ins_ori  || ins_xori  ||
                            ins_lb      || ins_lbu     || ins_lh   || ins_lhu   ||
                            ins_lw      || ins_mfc0    || ins_sc   || ins_ll    ||
                            ins_lwl     || ins_lwr      ;
    wire   rf_waddr_rd  =   ins_add     || ins_addu    || ins_sub  || ins_subu  ||
                            ins_slt     || ins_sltu    || ins_and  || ins_nor   ||
                            ins_or      || ins_xor     || ins_sllv || ins_sll   ||
                            ins_srav    || ins_sra     || ins_srlv || ins_srl   ||
                            ins_mfhi    || ins_mflo    || ins_clo  || ins_clz   ||
                            ins_movz    || ins_movn    || ins_mul  || ins_jalr   ;
    wire   rf_waddr_al  =   ins_bgezal  || ins_bltzal  || ins_jal  || ins_bgezall  || ins_bltzall ;
    assign link         =   ins_bgezal  || ins_bltzal  || ins_jalr || ins_jal   || ins_bgezall  || ins_bltzall ;
    assign rf_wen       =   rf_waddr_rt || rf_waddr_rd || rf_waddr_al;//Not these three write-back type, means no need to write back
    assign rf_waddr     =   rf_waddr_rt ? rt   :
                            rf_waddr_rd ? rd   :
                            rf_waddr_al ? `R31 : `ADDR_LEN'b0;
    //-----------------------------------------------------------------------------------------------------------





    //---------------------------------------Exception detection---------------------------------------
    //excp_ri:cause Reserved instruction exception
    //If the current decode instruction doesn't match the format of any preset instruction, excp_ri = 1
    //excp_sys:cause System call exception
    //If the current decode instruction is a syscall instruction, excp_sys = 1
    //excp_bp:cause Break point exception
    //If the current decode instruction is a break instruction, excp_bp = 1
    //eret:return from exception handler
    assign exc_ri       = !(ins_addi || ins_addiu || ins_slti   || ins_sltiu   ||
                            ins_andi || ins_lui   || ins_ori    || ins_xori    ||
                            ins_beq  || ins_bne   || ins_bgtz   || ins_blez    ||
                            ins_bgez || ins_bltz  || ins_bgezal || ins_bltzal  ||
                            ins_lb   || ins_lbu   || ins_lh     || ins_lhu     ||
                            ins_lw   || ins_sb    || ins_sh     || ins_sw      ||
                            ins_add  || ins_addu  || ins_sub    || ins_subu    ||
                            ins_slt  || ins_sltu  || ins_div    || ins_divu    ||
                            ins_mult || ins_multu || ins_and    || ins_nor     ||
                            ins_or   || ins_xor   || ins_sllv   || ins_sll     ||
                            ins_srav || ins_sra   || ins_srlv   || ins_srl     ||
                            ins_jr   || ins_jalr  || ins_mfhi   || ins_mflo    ||
                            ins_mthi || ins_mtlo  || ins_break  || ins_syscall ||
                            ins_j    || ins_jal   || ins_eret   || ins_mfc0    ||
                            ins_mtc0 || ins_beql  || ins_bgezall|| ins_bgezl   ||
                            ins_bgtzl|| ins_blezl || ins_bltzall|| ins_bltzl   ||
                            ins_bnel || ins_cache || ins_clo    || ins_clz     ||
                            ins_ll   || ins_sc    || ins_lwl    || ins_lwr     ||
                            ins_swl  || ins_swr   || ins_madd   || ins_maddu   ||
                            ins_msub || ins_msubu || ins_movn   || ins_movz    ||
                            ins_perf || ins_sync  || ins_wait   || ins_teq     ||
                            ins_teqi || ins_tge   || ins_tgei   || ins_tgeiu   ||
                            ins_tgeu || ins_tlt   || ins_tlti   || ins_tltiu   ||
                            ins_tltu || ins_tne   || ins_tnei   || ins_tlbp    ||
                            ins_tlbr || ins_tlbwi || ins_tlbwr  || ins_movft   ||
                            ins_cop1 || ins_lwc1  || ins_ldc1   || ins_swc1    ||
                            ins_sdc1 || ins_mul  );
    assign exc_sys      =   ins_syscall;
    assign exc_bp       =   ins_break;
    assign eret         =   ins_eret;

    //----------Send to the CP0 register----------
    //mtc0,mfc0:control to read from CP0 or write into CP0
    //cp0_addr:CP0_read_address,to identify which CP0 register to read data from or write into
    assign mtc0         =   ins_mtc0;
    assign mfc0         =   ins_mfc0;
    assign cp0_addr     =   {rd,sel};
    //-------------------------------------------------------------------------------------------------

endmodule
