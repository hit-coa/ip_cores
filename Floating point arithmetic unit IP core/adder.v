`timescale 1ns / 1ps

module adder(
    input             clk,
    input             reset,
    input             en,
    input      [63:0] a,
    input      [63:0] b,
    output /*reg*/        result_valid,
    output /*reg*/ [63:0] result
);

    // Control logic
    wire a_ready;
    wire b_ready;
    wire [63:0] result_d;
    wire        result_valid_d;

    /*always @ (posedge clk) begin
        if (reset) begin
            busy <= 1'b0;
        end
        else if (a_ready && b_ready && en) begin
            busy <= 1'b1;
        end
        else if (result_valid) begin
            busy <= 1'b0;
            result <= result_d;
        end
    end*/

    /*always @ (posedge clk) begin
        if (reset) begin
            result_valid <= 1'b0;
            result <= 64'b0;
        end
        else if (result_valid) begin
            result_valid <= result_valid_d;
            result <= result_d;
        end
    end*/


    floating_point_0 U_floating_point_0(
        .aclk                (clk         ),
        .aresetn             (~reset      ),
        .s_axis_a_tdata      (a           ),
        .s_axis_a_tready     (a_ready     ),//O
        .s_axis_a_tvalid     (en          ),
        .s_axis_b_tdata      (b           ),
        .s_axis_b_tready     (b_ready     ),//O
        .s_axis_b_tvalid     (en          ),
        .m_axis_result_tdata (result/*_d*/    ),
        .m_axis_result_tready(en          ),//I
        .m_axis_result_tvalid(result_valid/*_d*/)
    );

endmodule
