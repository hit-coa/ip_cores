`timescale 1ns / 1ps

`define BTB_FILE_PATH "C:/Users/admin/Desktop/branch_prediction/btb"
`define VALID_BIT   55
`define COND_BIT    54
`define TAG_BITS    53:32
`define TARGET_BITS 31:0


module btb(
    input         clk,
    input         reset,
    input  [9 :0] index_in,
    input  [21:0] tag_in,
    input         wen,
    input         wcond,
    input  [9 :0] windex,
    input  [21:0] wtag,
    input  [31:0] wtarget,
    output        hit,
    output [31:0] target
);

    // Define BTB
    reg [55:0] btb [255:0];

    // Update BTB
    always @ (posedge clk) begin
        if(reset) begin
            $readmemh(`BTB_FILE_PATH , btb);
        end
        else if(wen) begin
            btb[windex][`VALID_BIT  ]  <= 1'b1;
            btb[windex][`COND_BIT   ]  <= wcond;
            btb[windex][`TAG_BITS   ]  <= wtag;
            btb[windex][`TARGET_BITS]  <= wtarget;
        end
    end

    // Look up BTB
    assign hit          = btb[index_in][`VALID_BIT] && (btb[index_in][`TAG_BITS] == tag_in);
    assign target       = btb[index_in][`TARGET_BITS];

endmodule

