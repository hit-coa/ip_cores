`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/03/11 22:43:34
// Design Name: 
// Module Name: regfile
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "../Head_Files/Instructions.vh"
`include "../Head_Files/Parameters.vh"
`include "../Head_Files/Parameters_bus.vh"

module regfile
    #(
        parameter GPRS_NUM          = 32,   //Number of gprs
        parameter ZERO_ADDR         = 0     //Address of R0 register
    )
(
        //---------Control signal---------
        input                       clk,
        input                       resetn,
        //---------Write signal---------
        input                       rf_wen,
        input   [`ADDR_LEN-1:0]     rf_waddr,
        input   [`REG_WIDTH-1:0]    rf_wdata,
        //---------Read signal---------
        input   [`ADDR_LEN-1:0]     addr_reg_a,
        input   [`ADDR_LEN-1:0]     addr_reg_b,
        output  [`REG_WIDTH-1:0]    reg_a,
        output  [`REG_WIDTH-1:0]    reg_b
    );

    //Notice: Initial is not allowed
    //Notice: Specific regs: r0 -> 0 , r31 -> result of   or as a normal register
    //32 x 32-bit gprs
    reg [`REG_WIDTH-1:0]        r [GPRS_NUM-1:0];//32 x 32-bit gprs




    //---------------------------------------Write GPRs---------------------------------------
    //R0 can not be written
    integer i;
    always @ (posedge clk) begin
        //-----Initial GPRs-----
        if(!resetn) begin
            for (i = 0; i < GPRS_NUM; i = i + 1) begin
                r[i] = `REG_WIDTH'b0;
            end
        end

        //-----Write GPRs-----
        else if (rf_wen && (rf_waddr != `ADDR_LEN'b00000)) begin
            r[rf_waddr] <= rf_wdata;
        end
    end
    //---------------------------------------------------------------------------------------




    //---------------------------------------Read GPRs---------------------------------------
    //R0 is always 0
    assign reg_a = r[addr_reg_a];//reg_a -> [25:21]
    assign reg_b = r[addr_reg_b];//reg_b -> [20:16]
    //----------------------------------------------------------------------------------------


endmodule
